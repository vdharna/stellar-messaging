package com.stellar.messaging.quote.domain.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Quote implements Serializable {

	private static final long serialVersionUID = 1219636046415933346L;

	@Id
    @GeneratedValue
    private Long id;
    
    @Column( name = "quote_number")
    private String quoteNumber; 
    
    @Column( name = "date_quoted")
    @Temporal(TemporalType.DATE)
    private Calendar dateQuoted;
    
    @Column( name = "aircraft")
    private String aircraft;
    
    @Column( name = "tail_number")
    private String tailNumber;
    
    @Column( name = "trip_number")
    private String tripNumber;
   
	@Column(name = "fet")
	private BigDecimal fet;
	
	@Column(name = "sub_total")
	private BigDecimal subTotal;
	
	@Column(name = "grand_total")
	private BigDecimal grandTotal;
	
	@Column(name = "additional_comments")
	private String additionalComments;
	
    @Embedded
    @Column( name = "contact")
    private Contact contact;
    
    @ElementCollection
	@CollectionTable(name = "TRIP_LEG", joinColumns = @JoinColumn(name = "QUOTE_ID"))
    @Column( name = "legs")
    private List<TripLeg> legs;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getQuoteNumber() {
		return quoteNumber;
	}

	public void setQuoteNumber(String quoteNumber) {
		this.quoteNumber = quoteNumber;
	}

	public Calendar getDateQuoted() {
		return dateQuoted;
	}

	public void setDateQuoted(Calendar dateQuoted) {
		this.dateQuoted = dateQuoted;
	}

	public String getAircraft() {
		return aircraft;
	}

	public void setAircraft(String aircraft) {
		this.aircraft = aircraft;
	}

	public String getTailNumber() {
		return tailNumber;
	}

	public void setTailNumber(String tailNumber) {
		this.tailNumber = tailNumber;
	}

	public String getTripNumber() {
		return tripNumber;
	}

	public void setTripNumber(String tripNumber) {
		this.tripNumber = tripNumber;
	}

	public BigDecimal getFet() {
		return fet;
	}

	public void setFet(BigDecimal fet) {
		this.fet = fet;
	}

	public BigDecimal getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public BigDecimal getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}

	public String getAdditionalComments() {
		return additionalComments;
	}

	public void setAdditionalComments(String additionalComments) {
		this.additionalComments = additionalComments;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public List<TripLeg> getLegs() {
		return legs;
	}

	public void setLegs(List<TripLeg> legs) {
		this.legs = legs;
	}

}
