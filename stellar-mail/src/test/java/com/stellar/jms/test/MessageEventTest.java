package com.stellar.jms.test;

import javax.inject.Inject;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.stellar.messaging.event.domain.model.MessageEvent;
import com.stellar.messaging.event.domain.service.MessageEventService;
import com.stellar.messaging.infrastrucutre.cdi.Resources;
import com.stellar.messaging.infrastucture.jms.EmailMessageConsumer;
import com.stellar.messaging.interfaces.web.MessageBean;
import com.stellar.messaging.quote.domain.model.Quote;

@RunWith(Arquillian.class)
public class MessageEventTest {

	@Inject
	private MessageEventService msgEventService;

	@Deployment
	public static Archive<?> createTestArchive() {

		WebArchive war = ShrinkWrap
				.create(WebArchive.class, "stellar-messaging.war")
				.addPackage(MessageEvent.class.getPackage())
				.addPackage(MessageEventService.class.getPackage())
				.addPackage(Resources.class.getPackage())
				.addPackage(EmailMessageConsumer.class.getPackage())
				.addPackage(MessageBean.class.getPackage())
				.addPackage(Quote.class.getPackage())
				.addAsResource("META-INF/persistence.xml")
				.addAsResource("import.sql")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");

		System.out.println(war.toString(true));
		return war;

	}

	@Test
	public void test() {
		System.out.print(msgEventService);
	}

}
