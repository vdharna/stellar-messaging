package com.stellar.messaging.quote.domain.model;

public enum APCode {
	
	KTPL,
	KAUS,
	KSBA
}
