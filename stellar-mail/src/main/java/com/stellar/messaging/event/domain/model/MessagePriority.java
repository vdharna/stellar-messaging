package com.stellar.messaging.event.domain.model;

public enum MessagePriority {
	
	HIGH, MEDIUM, LOW;

}
