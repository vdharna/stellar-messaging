package com.stellar.messaging.infrastucture.jms;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Map;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import com.stellar.messaging.infrastrucutre.cdi.Log;

@Dependent
public class EmailTemplateHelper {
	
	@Inject
	private VelocityEngine ve;
	
	@Inject
	private VelocityContext context;
	
	public String transpose(Map<String, Object> properties) throws IOException{
		
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());

        ve.init();
        
        final String templatePath = "/templates/quote.vm";
        InputStream input = this.getClass().getClassLoader().getResourceAsStream(templatePath);
        if (input == null) {
            throw new IOException("Template file doesn't exist");
        }
        
        if (properties != null) {
            for (Map.Entry<String, Object> property : properties.entrySet()) {
                context.put(property.getKey(), property.getValue());
            }
        }
        
        Template template = ve.getTemplate(templatePath, "UTF-8");
        
        StringWriter messageContent = new StringWriter(); 
        template.merge(context, messageContent); 
        
        return messageContent.toString();
	}
	

}
