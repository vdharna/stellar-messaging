package com.stellar.messaging.event.domain.model;

import java.io.Serializable;
import java.util.UUID;

import javax.enterprise.context.Dependent;

@Dependent
public class MessageEvent implements Serializable {

	private UUID eventId;
	private Long quoteId;
	private MessagePriority priority;
	private TransportType transportType;
	
	public MessageEvent() {
		eventId = UUID.randomUUID();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public Long getQuoteId() {
		return quoteId;
	}

	public MessageEvent quoteId(Long quoteId) {
		this.quoteId = quoteId;
		return this;
	}

	public MessagePriority getPriority() {
		return priority;
	}

	public MessageEvent priority(MessagePriority priority) {
		this.priority = priority;
		return this;

	}

	public TransportType getTransportType() {
		return transportType;
	}

	public MessageEvent transportType(TransportType transportType) {
		this.transportType = transportType;
		return this;
	}

	public UUID getEventId() {
		return eventId;
	}

}
