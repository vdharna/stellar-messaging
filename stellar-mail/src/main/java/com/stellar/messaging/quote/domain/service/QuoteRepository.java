package com.stellar.messaging.quote.domain.service;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.stellar.messaging.quote.domain.model.Quote;

@Stateless
public class QuoteRepository {

	@PersistenceContext
	private EntityManager em;
	
	public Quote findById(Long id) {
		Query query = em.createQuery("select q from Quote q where q.id = :id");
		query.setParameter("id", id);
		Quote quote = (Quote) query.getSingleResult();
		return quote;
	}

}
