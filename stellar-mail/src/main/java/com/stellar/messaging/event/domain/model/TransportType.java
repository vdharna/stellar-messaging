package com.stellar.messaging.event.domain.model;

public enum TransportType {
	
	EMAIL, SMS;

}
