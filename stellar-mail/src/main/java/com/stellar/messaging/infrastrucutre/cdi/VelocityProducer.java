package com.stellar.messaging.infrastrucutre.cdi;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

@RequestScoped
public class VelocityProducer {

	@Produces
	public VelocityEngine getVelocityEngineInstance() {
		return new VelocityEngine();

	}

	@Produces
	public VelocityContext getVelocityContextInstance() {
		return new VelocityContext();
	}

}
