/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.stellar.messaging.interfaces.web;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;


import com.stellar.messaging.event.domain.model.MessageEvent;
import com.stellar.messaging.event.domain.model.MessagePriority;
import com.stellar.messaging.event.domain.model.TransportType;
import com.stellar.messaging.event.domain.service.MessageEventService;

/**
 * entry point for testing the mail subsystem
 * 
 */

@Named
@RequestScoped
public class MessageBean implements Serializable {

	private static final long serialVersionUID = 1544680932114626710L;

	@Inject
	private MessageEventService msgEventService;

	@Inject
	private MessageEvent event;

	/**
	 * Method to send the email based upon values entered in the JSF view.
	 * Exception should be handled in a production usage but is not handled in
	 * this example.
	 * 
	 * @throws Exception
	 */
	public void send() throws Exception {

		event.priority(MessagePriority.HIGH)
		.transportType(TransportType.EMAIL)		
		.quoteId(new Long(100));

		// send the message event
		msgEventService.send(event);
	}
}
