package com.stellar.messaging.infrastrucutre.cdi;

import java.util.logging.Logger;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@Interceptor
@Log
@Priority(Interceptor.Priority.PLATFORM_BEFORE)
public class LoggingInterceptor {

	// ======================================
	// = Attributes =
	// ======================================

	@Inject
	private Logger logger;

	// ======================================
	// = Business methods =
	// ======================================

	@AroundInvoke
	public Object logMethod(InvocationContext ic) throws Exception {

		logger.entering(ic.getTarget().toString(), ic.getMethod().getName());

		try {
			return ic.proceed();

		} catch (Exception e) {

			logger.severe("<<< Error encountered::: " + "Class::: "
					+ ic.getTarget().toString() + "::: Method ::: "
					+ ic.getMethod().getName() + " Error::: " + e.getMessage());
			throw e;

		} finally {

			logger.exiting(ic.getTarget().toString(), ic.getMethod().getName());
		}
	}
}