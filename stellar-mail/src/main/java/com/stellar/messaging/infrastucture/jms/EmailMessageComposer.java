package com.stellar.messaging.infrastucture.jms;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.activation.DataHandler;
import javax.activation.URLDataSource;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.stellar.messaging.event.domain.model.MessageEvent;
import com.stellar.messaging.quote.domain.model.Quote;
import com.stellar.messaging.quote.domain.service.QuoteRepository;

@RequestScoped
public class EmailMessageComposer {

	@Resource(mappedName = "java:jboss/mail/Default")
	private Session mailSession;

	@Inject
	private EmailTemplateHelper helper;

	@Inject
	private QuoteRepository quoteRepository;

	public void send(MessageEvent event) throws MessagingException, IOException {

		MimeMessage msg = new MimeMessage(mailSession);
		Address from = new InternetAddress(UUID.randomUUID().toString());
		Address[] to = new InternetAddress[] { new InternetAddress(UUID
				.randomUUID().toString()) };

		msg.setFrom(from);
		msg.setRecipients(Message.RecipientType.TO, to);
		msg.setSubject(UUID.randomUUID().toString());
		msg.setSentDate(new java.util.Date());

		Multipart multipartRelated = new MimeMultipart("related");
		MimeBodyPart mbp = new MimeBodyPart();

		// pull attributes of the customer / quote for the main body
		Map<String, Object> map = new HashMap<String, Object>();
		Quote quote = quoteRepository.findById(event.getQuoteId());
		map.put("quote", quote);
		String body = helper.transpose(map);

		mbp.setContent(body, "text/html");

		multipartRelated.addBodyPart(mbp);

		MimeBodyPart mbp2 = new MimeBodyPart();
		mbp2.setDataHandler(new DataHandler(
				new URLDataSource(
						new URL(
								"http://blog.signalnoise.com/wp-content/uploads/2011/07/i_jwjets.jpg"))));
		mbp2.setHeader("Content-ID", "<myimage>");

		multipartRelated.addBodyPart(mbp2);

		msg.setContent(multipartRelated);

		Transport.send(msg);

	}
}
