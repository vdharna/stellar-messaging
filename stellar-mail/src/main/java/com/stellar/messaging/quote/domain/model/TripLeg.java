package com.stellar.messaging.quote.domain.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class TripLeg implements Serializable {

	private static final long serialVersionUID = -2255375109467817595L;

	@Column(name = "leg_number")
	private int legNumber;

	@Column(name = "pax")
	private int pax;

	@Temporal(TemporalType.DATE)
	@Column(name = "departure_date")
	private Date date;

	@Column(name = "etd")
	private String etd;

	@Column(name = "eta")
	private String eta;

	@Enumerated(EnumType.STRING)
	@Column(name = "departure_city")
	private APCode departureCity;

	@Enumerated(EnumType.STRING)
	@Column(name = "arrival_city")
	private APCode arrivalCity;

	@Column(name = "abt")
	private String abt;
	
	@Column(name = "stat_miles")
	private int statMiles;

	public int getLegNumber() {
		return legNumber;
	}

	public void setLegNumber(int legNumber) {
		this.legNumber = legNumber;
	}

	public int getPax() {
		return pax;
	}

	public void setPax(int pax) {
		this.pax = pax;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getEtd() {
		return etd;
	}

	public void setEtd(String etd) {
		this.etd = etd;
	}

	public String getEta() {
		return eta;
	}

	public void setEta(String eta) {
		this.eta = eta;
	}

	public APCode getDepartureCity() {
		return departureCity;
	}

	public void setDepartureCity(APCode departureCity) {
		this.departureCity = departureCity;
	}

	public APCode getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCity(APCode arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

	public String getAbt() {
		return abt;
	}

	public void setAbt(String abt) {
		this.abt = abt;
	}

	public int getStatMiles() {
		return statMiles;
	}

	public void setStatMiles(int statMiles) {
		this.statMiles = statMiles;
	}
}

