package com.stellar.messaging.event.domain.service;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.Destination;
import javax.jms.JMSContext;
import javax.jms.JMSDestinationDefinition;
import javax.jms.JMSDestinationDefinitions;

import com.stellar.messaging.event.domain.model.MessageEvent;
import com.stellar.messaging.infrastrucutre.cdi.Resources;

@JMSDestinationDefinitions(
		{ @JMSDestinationDefinition(name = Resources.EMAIL_QUEUE_NAME, 
		interfaceName = "javax.jms.Queue", 
		resourceAdapter = "jmsra", 
		description = "email queue for sending email events"),
		
		@JMSDestinationDefinition(name = Resources.SMS_QUEUE_NAME, 
		interfaceName = "javax.jms.Queue", 
		resourceAdapter = "jmsra", 
		description = "email queue for sending SMS events") 
})
@Stateless
public class MessageEventService {

	@Inject
	private JMSContext context;
	
	@Resource(mappedName = Resources.EMAIL_QUEUE_NAME)
	private Destination emailQueue;
	
	@Resource(mappedName = Resources.SMS_QUEUE_NAME)
	private Destination smsQueue;

	
	public void send(MessageEvent event) {
		
		switch (event.getTransportType()) {
		
		case EMAIL:
			context.createProducer().send(emailQueue, event);
			break;
			
		case SMS:
			context.createProducer().send(smsQueue, event);
			break;
			
		default:
			break;
		}
		
	}
	
}
