package com.stellar.messaging.infrastucture.jms;

import java.io.IOException;
import java.util.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.mail.MessagingException;

import com.stellar.messaging.event.domain.model.MessageEvent;
import com.stellar.messaging.infrastrucutre.cdi.Log;
import com.stellar.messaging.infrastrucutre.cdi.Resources;

@Log
@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = Resources.EMAIL_QUEUE_NAME),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"), })
public class EmailMessageConsumer implements MessageListener {

	@Inject
	private EmailMessageComposer composer;

	@Inject
	private Logger logger;

	@Override
	public void onMessage(Message message) {
		
		try {

			int deliveryCount = message.getIntProperty("JMSXDeliveryCount");

			if (deliveryCount < 10) {

				MessageEvent msgEvent = message.getBody(MessageEvent.class);
				logger.severe("Processing Message Event: "
						+ msgEvent.getEventId());
				composer.send(msgEvent);

			} else {

				// message has been redelivered ten times,
				// let's do something to prevent endless redeliveries
				// such as sending it to dead message queue
				// details omitted
				logger.severe("NEED TO SEND TO ERROR QUEUE");
			}

		} catch (JMSException jmsex) {

			throw new RuntimeException(jmsex.getMessage());

		} catch (MessagingException msgex) {

			throw new RuntimeException(msgex.getMessage());

		} catch (IOException ioex) {

			throw new RuntimeException(ioex.getMessage());

		}
	}

}
